/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author USUARIO
 */
public class empleado_fijo {
    private String nombre;
    private Integer edad;
    private String departamento;
    private String anio_ingreso; 
    private String anio_salida;
    private int sueldo_fijo;
    private int bono_anual;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getAnio_ingreso() {
        return anio_ingreso;
    }

    public void setAnio_ingreso(String anio_ingreso) {
        this.anio_ingreso = anio_ingreso;
    }

    public String getAnio_salida() {
        return anio_salida;
    }

    public void setAnio_salida(String anio_salida) {
        this.anio_salida = anio_salida;
    }

    public int getSueldo_fijo() {
        return sueldo_fijo;
    }

    public void setSueldo_fijo(int sueldo_fijo) {
        this.sueldo_fijo = sueldo_fijo;
    }

    public int getBono_anual() {
        return bono_anual;
    }

    public void setBono_anual(int bono_anual) {
        this.bono_anual = bono_anual;
    }
    

}
